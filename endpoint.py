from flask import Flask, request
from flask_restx import Api, Resource

from chat_agent import agent

app = Flask(__name__)

api = Api(
    app=app,
    doc="/docs",
    basePath="/",
    version="1.0.0",
    title="HTWK Support Bot",
    termsOfService="https://tech4comp.de/",
    description="The goal of this chatbot is to support students with organizational questions.",
)


@api.route("/generate_response", endpoint="generate_response")
class GenerateResponse(Resource):
    @api.doc(params={"msg": "A question to be answered"}, id="generate_response")
    @api.response(200, "Success")
    def post(self):
        if "msg" in request.form:
            prompt = request.form["msg"]
        else:
            prompt = request.json.get("msg")

        executer = agent()
        response = executer.invoke(
            {
                "input": prompt,
                "chat_history": [],
            },
        )
        answer = {"message": response["output"], "closeContext": "false"}
        return answer


if __name__ == "__main__":
    app.run(debug=True)

import json

import requests
import streamlit as st

API_URL = "https://htwk-support-bot.tech4comp.dbis.rwth-aachen.de/generate_response"

st.title("HTWK Studi Support Bot")

if "messages" not in st.session_state:
    st.session_state.messages = []

for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

if human_prompt := st.chat_input("Ask a question here!"):
    with st.chat_message("user"):
        st.markdown(human_prompt)
    st.session_state.messages.append({"role": "user", "content": human_prompt})

if human_prompt is not None:
    question = {"msg": human_prompt}
    response = requests.post(API_URL, data=question)

    try:
        answer = json.loads(response.text)["message"]
    except:
        answer = "There is currently something wrong with the Chatbot."

    with st.chat_message("ai"):
        st.markdown(answer)

    st.session_state.messages.append({"role": "ai", "content": answer})

import os

from langchain.agents import AgentExecutor, ConversationalChatAgent, Tool
from langchain_openai import ChatOpenAI

from tool_utils import *

openai_api_key = os.environ["OPENAI_API_KEY"]
llm = ChatOpenAI(api_key=openai_api_key, model="gpt-3.5-turbo", temperature=0)

SYSTEM_PROMPT = """\
Du bist ein Assistent für Studenten des Studiengangs Informatik der Hochschule für Technik, Wirtschaft und Kultur (HTWK) Leipzig. Deine Aufgabe ist es den Studenten bei organisatorischen und nicht-fachlichen Fragen zu helfen.
Dafür musst du IMMER auf deutsch antworten..
"""


def agent() -> AgentExecutor:
    tools = [
        Tool(
            name="Studiengang",
            func=run_studiengang_chain_llama_index,
            description="""
            Dieses Tool beinhaltet allgemeine Informationen zu dem Bachelorstudiengang Informatik der HTWK Leipzig.
            Es beinhaltet die Studienordnung des Bachelorstudiengangs Informatik und eine Kontaktperson des Studienamtes.
            """,
            return_direct=True,
        ),
        Tool(
            name="Prüfungen",
            func=run_prüfungen_chain_llama_index,
            description="""
            Dieses Tool beinhaltet KEINE Informationen zu spezifischen Prüfungen. Benutze dieses Tool nicht wenn direkt nach einem Modul gefragt wird.
            Es beinhaltet Informationen zu dem Ablegen von Prüfungen.
            Es beinhaltet Informationen zu der Anmeldung, Abmeldung und Zulassung zu Prüfungen.
            Es beinhaltet allgemeine Informationen zu dem Abschlussmodul mit der Bachelorarbeit und Masterarbeit.
            Es beinhaltet allgemeine Informationen zu der Praxisphase und dem Auslandsaufenthalt.
            Es beinhaltet allgemeine Informationen zu den Prüfungsergebnissen.
            Es beinhaltet allgemeine Informationen zu den Prüfungsterminen und Abgabefristen.
            Es beinhaltet allgemeine Informationen zu dem Rücktritt, Krankheit und Versäumnis.
            Es beinhaltet allgemeine Informationen zu der Wiederholung.
            Es beinhaltet die Prüfungsordnung des Bachelorstudiengangs Informatik.
            Dieses Tool beinhaltet Kontaktdaten zu einer Person des Prüfungsamts und des Prüfungsausschuss.
            Dieses Tool beinhaltet ausschließlich Informationen zu den genannten Punkten.
            """,
            return_direct=True,
        ),
        Tool(
            name="Praktikum",
            func=run_praktikum_chain_llama_index,
            description="""
            Dieses Tool beinhaltet allgemeine Informationen zu den Praktikum des Bachelorstudiengangs Informatik der HTWK Leipzig.
            Es beinhaltet Information zu einer Kontaktperson des Praktikantenamtes.
            """,
            return_direct=True,
        ),
        Tool(
            name="Prüfungsplan",
            func=run_pruefungsplan_chain_llama_index,
            description="""
            Dieses Tool beinhaltet genaue Informationen zu den Prüfungen.
            Es beinhaltet den Studiengang des Moduls.
            Es beinhaltet die Modulnummer des Moduls.
            Es beinhaltet den Namen des Moduls.
            Es beinhaltet die Form des Moduls.
            Es beinhaltet die Form der Modulprüfung.
            Es beinhaltet den Erstprüfer der Modulprüfung.
            Es beinhaltet den Zweitprüfer der Modulprüfung.
            Es beinhaltet das Datum der Modulprüfung.
            Es beinhaltet die Zeit der Modulprüfung.
            Es beinhaltet die Dauer der Modulprüfung.
            Es beinhaltet den Raum der Modulprüfung.
            Es beinhaltet weitere Angaben zu der Modulprüfung.
            Es beinhaltet die Abmeldefrist der Modulprüfung.
            Dieses Tool beinhaltet ausschließlich Informationen zu den genannten Punkten.
            """,
            return_direct=True,
        ),
        Tool(
            name="Mathematik für Informatiker II",
            func=run_mathematik_für_informatiker_ii_index,
            description="""
            Dieses Tool beinhaltet allgemeine Informationen zu der Prüfung des Moduls Mathematik für Informatiker II.
            Es beinhaltet das Ziel des Moduls.
            Es beinhaltet Informationen zu den Belegaufgaben des Moduls.
            Es beinhaltet Informationen zu der Prüfung des Moduls.
            Es beinhaltet Informationen zu der Benotung der Prüfung.
            Es beinhaltet Informationen zu den erlaubten Hilfsmitteln der Prüfung.
            """,
            return_direct=True,
        ),
    ]

    chat_agent = ConversationalChatAgent.from_llm_and_tools(
        llm=llm,
        tools=tools,
        system_message=SYSTEM_PROMPT,
    )

    executor = AgentExecutor.from_agent_and_tools(
        agent=chat_agent,
        tools=tools,
        return_intermediate_steps=True,
        handle_parsing_errors=True,
        max_execution_time=10,
        verbose=True,
    )

    return executor

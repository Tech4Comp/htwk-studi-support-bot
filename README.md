# HTWK Studi Support Bot

The goal of this chatbot is to support students with organizational questions.

The endpoint is located in `endpoint.py`. A streamlit application which accesses this endpoint is implemented in `app.py`.
The chatbot functionalities split up between `chat_agent.py` and `tools_utils.py`.
The context information accessible for the chatbot is stored in `data/`. Related topics are in the appropriate subfolders.
Loaded and indexed documents are stored in `storage/data/`
A documentation is available in this repository under `docs/`. Additional information can be found there.

# Use the chatbot

The deployed Chatbot is accessible via `https://htwk-support-bot.tech4comp.dbis.rwth-aachen.de/generate_response`.
The streamlit application `app.py` can be used to establish a connection. To get this running follow the following steps:

Make sure you have [python](https://www.python.org/downloads/release/python-3121/) installed.
1. Clone this repository
2. Create a virtual environment
   - `python -m venv .venv`
3. Enter enviroment
   - macos/linux: `source .venv/bin/activate`
   - windows: `.venv\Scripts\activate`
4. Install dependencies
   - `pip install streamlit`
5. Start streamlit application
   - `streamlit run app.py`


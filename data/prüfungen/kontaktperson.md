# Kontaktperson des Prüfungsausschuss

Prof. Dr. rer. nat. habil Michael Frank
Informatikstudiengänge (FIM-INF) 
Raum: [Z 409](https://www.htwk-leipzig.de/hochschule/standorte-lageplan/ueber-unsere-gebaeude/zuse-bau)
Telefon: +49 341 3076-6398
e-Mail: michael.frank@htwk-leipzig.de

# Kontaktperson des Prüfungsamts

Dipl.-Ing. Cindy Dräger
Informatikstudiengänge (FIM-INF) 
Raum: [TR (Haus C) 0.79](https://www.htwk-leipzig.de/hochschule/standorte-lageplan/ueber-unsere-gebaeude/trefftz-bau-haus-c)
Gebäude: Gebäude T (Haus C)
Telefon: +49 341 3076-8453
FAX: +49 341 3076-6507
e-Mail: pruefungsamt.fim-inf@htwk-leipzig.de
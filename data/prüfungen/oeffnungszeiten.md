# Oeffnungszeiten Zentrales Pruefungsamt

## Offene Sprechzeiten

Dienstag 9.00 – 12.00 Uhr und 13.00 – 16.00 Uhr
Donnerstag 9.00 – 12.00 Uhr

## Termine außerhalb der offenen Sprechzeiten

Für längere persönliche bzw. telefonische Beratungsgespräche vereinbaren Sie bitte direkt einen Termin mit der jeweiligen Ansprechperson.

# Addresse

Eichendorffstraße 14, 04277 Leipzig
# Präsenzprüfungen

Studierende sollten mindestens 15 Minuten vor Prüfungsbeginn der mündlichen oder schriftlichen Prüfung erscheinen.

Vor jeder Prüfung wird die Anwesenheit durch Unterschrift auf der Prüflingsliste bestätigt. Studierende müssen sich mit ihrer HTWK-Card oder Personalausweis ausweisen.

Sollten Sie nicht auf der Prüflingsliste stehen, können Sie nicht an der Prüfung teilnehmen! Wenden Sie sich bitte umgehend an Ihr Prüfungsamt.

Unmittelbar vor Prüfungsbeginn erfolgt eine Belehrung und die Aufklärung über erlaubte Hilfsmittel.

Hilfsmittel werden in der Regel bereits während der Vorlesungszeit durch den Prüfer bekannt gegeben.

# Projekte, Belege oder Hausarbeiten

Ihre Dozentin oder Ihr Dozent klärt mit Ihnen die Modalitäten wie Ausgabe des Themas, Form, Bearbeitungszeit oder Abgabetermin.

Das Abschlussmodul umfasst eine schriftliche Bachelor- oder Masterarbeit, in der Studierende zeigen, dass sie in der Lage sind, ein fachspezifisches Problem innerhalb einer festgelegten Bearbeitungszeit nach wissenschaftlichen Methoden zu bearbeiten. Zum Abschlussmodul gehört in der Regel eine Verteidigung bzw. ein Kolloquium in Form einer mündlichen Prüfung.

Für Bachelor- oder Mastermodule treffen die Studien- und Prüfungsordnung besondere Regelungen, was Zulassung, Themenwahl, Betreuung, Abgabe und Bewertung betrifft.

Themen und Anregungen finden Sie unter anderem in der [Bibliothekssammlung für Abschlussarbeiten](https://katalog.bib.htwk-leipzig.de/discovery/collectionDiscovery?vid=49LEIP_HTWK:49LEIPHTWK_VU1&collectionId=8169385240002586) oder im [Stellenportal der HTWK Leipzig](https://stellenticket.htwk-leipzig.de/de/offers/search/worktype/125/).

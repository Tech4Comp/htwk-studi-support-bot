# Prüfungsanmeldung

Die Prüfungsanmeldung erfolgt für die Pflicht- und Wahlpflichtmodule, in denen Sie eingeschrieben sind, in der Regel automatisch durch Ihr Studien- und Prüfungsamt.

Während der studienintegrierten Praxisphase oder bei Beurlaubung müssen Sie sich selbst zur Prüfung anmelden ([→ Formular Anmeldung zur Prüfung](https://www.htwk-leipzig.de/fileadmin/portal/htwk/studieren/download/Antrag_Pruefungsanmeldung.pdf)). Die Fristen entnehmen Sie bitte dem [Prüfungsplan](https://www.htwk-leipzig.de/studieren/im-studium/pruefungen/pruefungsplan). 

Achtung: Zweite Wiederholungsprüfungen müssen vorher beantragt werden!

Zu welchen Prüfungen Sie angemeldet sind, erfahren Sie im [QIS](https://www.htwk-leipzig.de/studienportal).

# Abmeldung von der Prüfung

Wollen Sie eine Prüfung, zu der Sie angemeldet sind, nicht im aktuellen Semester ablegen, können Sie sich per Antrag von der Prüfung abmelden. Nutzen Sie für Erstprüfungen bitte die Abmeldefunktion in QIS und andernfalls das [→ Formular Abmeldung von der Prüfung](https://www.htwk-leipzig.de/fileadmin/portal/htwk/studieren/download/Antrag_Pruefungsabmeldung.pdf). Die Abmeldung muss fristgerecht (siehe [Prüfungsplan](https://www.htwk-leipzig.de/studieren/im-studium/pruefungen/pruefungsplan)) erfolgen bzw. im Prüfungsamt vorliegen.

# Zulassung zu einer Prüfung

Die Zulassung wird erteilt, wenn keine Zulassungshindernisse vorliegen, insbesondere wenn die erforderlichen Prüfungsvorleistungen erbracht wurden. Erst dann dürfen Sie an der Prüfung teilnehmen. Ihre Dozentin oder ihr Dozent informiert Sie, ob Sie zugelassen sind.

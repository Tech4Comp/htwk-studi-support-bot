Nicht bestandene Prüfungenleistungen können maximal zweimal und nur innerhalb gewisser Fristen wiederholt werden.

Ist die Modulprüfung durch Kompensation  bestanden, d. h. eine nicht bestandene Prüfungsleistung  ergibt mit einer guten anderen Prüfungsleistung eine Modulnote von 4,0 oder besser, kann die nicht bestandene Prüfungsleistung nicht mehr zur Notenverbesserung wiederholt werden.

Für die Zweite Wiederholungsprüfung (3. Prüfungsversuch) ist ein fristgemäßer Antrag beim zuständigen Prüfungsausschuss zu stellen ([Antrag auf zweite Wiederholungsprüfung](https://www.htwk-leipzig.de/fileadmin/portal/htwk/studieren/download/Antrag_zweite_Wiederholungspruefung.pdf)).

Prüfungsvorleistungen können hingegen beliebig oft wiederholt werden.

Bitte beachten Sie, dass eine Abmeldung, eine fehlende Zulassung oder eine anerkannte Krankmeldung so wirken, als hätte die Prüfung "nicht stattgefunden". Es zählt nicht als Prüfungsversuch wie eine mit Fünf bewertete, nicht bestandene Prüfung.

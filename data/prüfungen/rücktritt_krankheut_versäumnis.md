

Können Sie an einer Prüfung kurzfristig nicht teilnehmen oder haben Sie eine Prüfung versäumt, müssen Sie von der Prüfung unverzüglich unter Angabe des Prüfungstermins und des Grundes zurücktreten.
Wird der geltend gemachte Grund vom Prüfungsausschuss anerkannt, gilt die Prüfung als nicht unternommen. Im Krankheitsfall wird die Prüfungsunfähigkeit über ein ärztliches Attest nachgewiesen.

Können Sie wegen Krankheit die Abgabefrist eine ortsungebundenen Prüfung nicht einhalten, können Sie die Verlängerung der Bearbeitungsdauer beantragen.

WICHTIG: Prüfungen gelten als nicht bestanden und werden mit der Note Fünf bewertet, sofern Studierende zu einer Prüfung unentschuldigt nicht erscheinen, kein Prüfungsergebnis innerhalb der vorgegebenen Frist abgeliefert wird oder beantragte Rücktritte oder Verlängerungen nicht genehmigt werden. 

Weitere Informationen finden Sie unter dem Menüpunkt "Rücktritt, Krankheit und Versäumnis"

Sind Studierende erkrankt und können daher nicht an einer Prüfung teilnehmen, bestehen folgende Möglichkeiten:

1) Sie werden automatisch angemeldet und können sich noch innerhalb der Fristen lt. Prüfungsplan abmelden: Abmeldung über QIS oder Formular Prüfungsabmeldung.

2) Es besteht keine Abmeldemöglichkeit mehr für eine Präsenzprüfung (z.B. Klausur oder mündliche Prüfung) und Sie treten wegen Krankheit von der Prüfung zurück: Formular Prüfungsrücktritt.

3) Es besteht keine Abmeldemöglichkeit für eine ortsungebundene Prüfung (Hausarbeiten, Belege, Projekte). Stattdessen beantragen Sie eine: Verlängerung der Abgabefrist.

Im Falle chronischer Erkrankungen können Sie vor der Prüfung gegebenenfalls einen Nachteilsausgleich beantragen.

Achtung: Prüfungen, zu denen Sie unentschuldigt fehlen oder deren festgelegte Bearbeitungszeit überschritten wird, gelten als nicht bestanden und werden mit Note 5 bewertet.

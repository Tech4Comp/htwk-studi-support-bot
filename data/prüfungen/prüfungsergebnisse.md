Eine Prüfung kann bestanden (Bewertung von 1,0 bis 4,0) oder nicht bestanden (Bewertung 5,0) sein.
Besteht eine Prüfung aus mehreren Teilleistungen, werden diese zu einer Modulnote zusammengerechnet. Die genaue Zusammensetzung und Gewichtung regelt die [Studien- und Prüfungsordnung](https://www.htwk-leipzig.de/studieren/studiengaenge/studien-pruefungsordnungen).

Die Prüfungsergebnisse werden im [Studienportal](https://www.htwk-leipzig.de/studienportal) veröffentlicht. Bei einer mündlichen Prüfung erfolgt die Bekanntgabe unmittelbar nach Beendigung der Prüfung.
Danach richten sich die Fristen für Widerspruch oder Beantragung einer Zweiten Wiederholungsprüfung.

Studierende haben die Möglichkeit Einsicht in ihre Prüfungsunterlagen zu nehmen. Termine hierfür werden individuell mit der Prüferin bzw. dem Prüfer vereinbart.

Die amtlichen Leistungsnachweise mit Unterschrift und Stempel für Bewerbungen, Praktika oder Anträge erhalten Sie zu den Sprechzeiten im Prüfungsamt.

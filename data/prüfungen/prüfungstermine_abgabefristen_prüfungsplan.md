# Prüfungen der Prüfungsperdiode (Prüfungszeitraum)

Mündliche Prüfungen oder Klausuren (beides Präsenzprüfungen abgekürzt mit PM und PK) finden in der Regel in der Prüfungsperiode des laufenden Semesters statt.

Das Studien- und Prüfungsamt veröffentlicht die genauen Prüfungstermine mit Prüfungsname, Raum und Uhrzeit im sogenannten [Prüfungsplan](https://www.htwk-leipzig.de/studieren/im-studium/pruefungen/pruefungsplan). Er ist online oder über Aushänge der Fakultät einsehbar. Ihre persönlichen Prüfungsanmeldungen finden Sie in [QIS](htwk-leipzig.de/qis).

Tipp: Der [Akademische Kalender](https://www.htwk-leipzig.de/studieren/im-studium/akademischer-kalender) enthält die Daten zur Vorlesungs- und Prüfungszeit.

# Prüfungen im gesamten Semester

Prüfungstermine für Projektarbeiten, Laborarbeiten und Referate werden als integraler Bestandteil einer Lehrveranstaltung in der Regel im Verlauf der Vorlesungszeit absolviert. Insbesondere die Prüfungsvorleistungen sind vor der Prüfungsperiode zu erbringen, um die Zulassung für die Prüfung im Prüfungszeitraum zu erhalten.

Um die Arbeitslast für die Studierenden über die Vorlesungszeit hinaus auf das gesamte Semester zu verteilen, können die Prüfungsleistungen Hausarbeiten und Belege bis zum Ende des Semesters abgeben werden, in dem das jeweilige Modul absolviert wird.

Über die Modalitäten informiert Sie Ihre Dozentin oder ihr Dozent.
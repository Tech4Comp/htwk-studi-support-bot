#!/bin/sh
NAMESPACE=htwk

POD_NAME=$(kubectl get pods -n htwk -o json | jq '.items[] | .metadata.name' | grep -m 1 'htwk-support-bot' | sed 's/"//g')

if [ -z "$POD_NAME" ]; then
    echo "Pod not found or empty. Exiting..."
    exit 1
fi

kubectl delete pod $POD_NAME -n htwk
# Data

This page gives you an overview on how to add, update or remove data from or to the Chatbot.

## The data folder

The `data/` folder contains all data available for the chatbot. To keep it clean and clear, related documents should be in the same sub folders. The idea is to split up the information in similar topic since not all information can be added to the prompt at once. So that the prompt always contains all information which are related to the topic of the question.

## Adding Data to the Chatbot

There are a few steps necessary to add new data to the chatbot.
1. Create a folder in `data/`
2. Place in the documents you want to add, this can be PDF, csv or markdown. Other types, like HTML, should also work, but are not tested.

After that you need to add it in the code.
In the `agent()` function inside `chat_agent.py` is a list containing all tools. Here you need to add your new tool.
```python
tools = [
    ...
    Tool(
        name="your_tool_name" # preferably the folder name from step 1
        func=run_your_tool_name_chain_llama_index # the function which gets called when this tool is accessed,
        description="""
        A good description about the information this tool provides. 
        """, # describe the content of this tool, aswell as when to use it and optionally how to use it
        return_direct=True, # to return the output of the tool directly
    ),
]
```

In the next part you need to implement the function of your tool. For this, go into `tools_util.py` and add the function there.

```python
# your_tool_name
def run_praktikum_chain_llama_index(query: str) -> str:
    llm_predictor = LLMPredictor(llm=llm)

    service_context = ServiceContext.from_defaults(
        llm_predictor=llm_predictor, callback_manager=callback_manager
    )

    set_global_service_context(service_context)

    retriever = _get_retriever(folder_name="your_folder_name") # put in the name of your created folder

    chain = retriever.as_query_engine(
        service_context=service_context,
        similarity_top_k=3,
        verbose=True,
        text_qa_template=DEFAULT_TEXT_QA_PROMPT_CUSTOM,
    )

    results = chain.query(query)

    final_answer = str(results)

    llama_debug.flush_event_logs()
    return final_answer

```

After this, your tool is accessible for the Chatbot.

## Removing Data from the Chatbot

To remove any data from the chatbot the only step needed is to remove the `Tool` from the `tools` list inside `agent()` in `chat_agent.py`. This is enough to remove the tool from the Chatbot. 

If you want to permanently remove the tool, it is appreciated to also remove the corresponding function as well as the folder containing the files.

## Updating Data

For the example, lets assume you want to update the Prüfungsplan.

To update already implemented data follow these steps:

1. Remove the old files from inside the corresponding folder, 
    - e.g. delete all files inside `data/prüfungsplan/`
2. Add the updated files to the same folder
    - e.g. place new files inside `data/prüfungsplan/`
3. Delete the index, if stored
    - e.g. delete `stored/data/prüfungsplan_index`
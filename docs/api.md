# API

This page explains the API of the Chatbot.

## Access the Endpoint

The only available endpoint is `/generate_response` and only accepts POST-Requests. It expects an attached body containing either form-data/multidata with the `msg` key or a json also with a `msg` key.

An example of how to connect to the API can be found in `app.py`.

## Implementation

The API is build with [Flask](https://flask.palletsprojects.com/en/3.0.x/) and [Flask-RESTX](https://flask-restx.readthedocs.io/en/latest/). It only has one endpoint, `POST /generate_response`, expecting the user's question.
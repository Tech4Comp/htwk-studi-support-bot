# Large Language Model

This page explains the usage of the llm's.

## Usage

For the Chatbot 2 instances of a llm are needed.
One for the selection of the correct tool, this one in initialized in `chat_agent.py`.
The second one is for the querying of the selected data, this on is located in `tools_utils.py`.

## Exchanging the model

Currently both are instances of the GPT-3.5-Turbo model from OpenAI. To upgrade the OpenAI model or swap to a fine-tuned model, the `model` parameter needs to be exchanged.

In case of switching away from the usage of the OpenAI API, instead of using the class `ChatOpenAI`, select the chat model you want to switch to. An overview can be found in the [LangChain Documentation](https://python.langchain.com/docs/integrations/chat/).

```python
llm = ChatOpenAI( # swap this with another class
    api_key=openai_api_key, 
    model="gpt-3.5-turbo", # swap this with a fine-tuned version or another pre-trained GPT model
    temperature=0, # to force the llm for a consistent output
)
```

Be aware of potential token and rate limits of other models.
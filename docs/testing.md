# Testing

This page explains how to test this application.

## Access the deployed API

The tech4comp hosted API can be accessed at `https://htwk-support-bot.tech4comp.dbis.rwth-aachen.de/generate_response`.

The easiest way to test is to use the Streamlit application in `app.py`. For this follow these steps before:

Make sure you have [python](https://www.python.org/downloads/release/python-3121/) installed.
1. Clone this repository
2. Create a virtual environment
3. Enter environment
   - macos/linux: `source .venv/bin/activate`
   - windows: `.venv\Scripts\activate`
4. Install Streamlit
   - `pip install streamlit`

Now you can start the application with `streamlit run app.py`. Afterwards a new tab should open in your web browser where you can access the chatbot.

Alternatively you can use a tool like [Postman](https://www.postman.com/) or [curl](https://curl.se/) to access this endpoint.

With a form-data/multiform body:

```bash
curl -X POST \
  https://htwk-support-bot.tech4comp.dbis.rwth-aachen.de/generate_response \
  -H 'Content-Type: multipart/form-data' \
  -F 'msg=Your question here'
```

or with a JSON body:

```bash
curl -X POST \
  https://htwk-support-bot.tech4comp.dbis.rwth-aachen.de/generate_response \
  -H 'Content-Type: application/json' \
  -d '{
    "msg": "Your question here"
}'
```

## Host the API yourself

You can either host the API inside a container or directly on your system.

### Docker

Make sure you have [Docker](https://www.docker.com/) installed.

1. Clone this repository
2. Open a terminal inside of the directory
3. Build the container
   - `docker build -t chatbot .`
4. Start the container
   - `docker run -e SERVER_PORT=9090 -p 9090:5000 -e OPENAI_API_KEY=$OPENAI_API_KEY chatbot`

The API is now accessible for requests under `http://127.0.0.1:9090/generate_response`. You can reach this API again via curl or Postman, alternatively you can swap out the `API_URL` inside of the `app.py` to use the streamlit application with your local API.

### Locally on your System

Make sure you have [Python](https://www.python.org/downloads/release/python-3121/) installed.

1. Clone this repository
2. Create a virtual environment
   - `python -m venv .venv`
3. Enter environment
   - macos/linux: `source .venv/bin/activate`
   - windows: `.venv\Scripts\activate`
4. Install dependencies
   - `pip install -r requirements.txt`
5. Start the server
   - `python -m flask --app endpoint run`

The API is now accessible for requests under `http://127.0.0.1:5000/generate_response`. You can reach this APU again via curl or Postman, alternatively you can swap out the `API_URL` inside of the `app.py` to use the streamlit application with your local API.
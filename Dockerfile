FROM python:3.11

WORKDIR /app

ENV OPENAI_API_KEY=""

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python3", "-m", "flask", "--app", "endpoint", "run", "--host=0.0.0.0" ]

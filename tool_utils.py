import glob
import os

from langchain_community.document_loaders import (
    CSVLoader,
    UnstructuredFileLoader,
    UnstructuredPDFLoader,
)
from langchain_openai import ChatOpenAI
from llama_index import (
    Document,
    LLMPredictor,
    Prompt,
    ServiceContext,
    StorageContext,
    VectorStoreIndex,
    load_index_from_storage,
    set_global_service_context,
)
from llama_index.callbacks import CallbackManager, LlamaDebugHandler
from llama_index.indices.base import BaseIndex

llama_debug = LlamaDebugHandler(print_trace_on_end=False)
callback_manager = CallbackManager([llama_debug])

openai_api_key = os.environ["OPENAI_API_KEY"]
llm = ChatOpenAI(api_key=openai_api_key, model="gpt-3.5-turbo", temperature=0)

_DEFAULT_TEXT_QA_PROMPT_TMPL = """\
Context information and documents are below.

<context>
{context_str}
</context>

Given the context information and documents above. You must STRICTLY answer the question in GERMAN and provide a detailed answer based only on the context information and provided documents above. The only verified documents are between the context-tags. Answer solely based on these documents. You must ALWAYS try to formulate an answer based only on these documents, ALWAYS answer in german. If you can not find an direct answer in these documents, then say so.

<question>
{query_str}\n
</question>
"""

DEFAULT_TEXT_QA_PROMPT_CUSTOM = Prompt(_DEFAULT_TEXT_QA_PROMPT_TMPL)


def _get_retriever(folder_name: str) -> BaseIndex | VectorStoreIndex:
    paths = glob.glob(f"data/{folder_name}/*")
    storage_dir = f"storage/data/{folder_name}_index"

    llm_predictor = LLMPredictor(llm=llm)

    service_context = ServiceContext.from_defaults(llm_predictor=llm_predictor)
    set_global_service_context(service_context)

    try:
        context = StorageContext.from_defaults(persist_dir=storage_dir)
        retriever = load_index_from_storage(context)
    except:
        pages = []
        for path in paths:
            if path.endswith(".csv"):
                loader = CSVLoader(file_path=path)
            elif path.endswith(".pdf"):
                loader = UnstructuredPDFLoader(
                    path,
                    strategy="fast",
                    mode="single",
                )
            else:
                loader = UnstructuredFileLoader(
                    path,
                    strategy="fast",
                    mode="single",
                )

            page = loader.load()
            pages.extend(page)

        documents = []
        for page in pages:
            document = Document(
                text=page.page_content,
                metadata={"file_name": page.metadata["source"].split("/")[-1]},
                metadata_seperator="::",
                metadata_template="{key}=>{value}",
                text_template="Metadata: {metadata_str}\n-----\nContent: {content}",
            )
            documents.append(document)

        retriever = VectorStoreIndex.from_documents(documents)
        retriever.storage_context.persist(storage_dir)
    return retriever


# Pruefungsplan
def run_pruefungsplan_chain_llama_index(query: str) -> str:
    llm_predictor = LLMPredictor(llm=llm)

    service_context = ServiceContext.from_defaults(
        llm_predictor=llm_predictor, callback_manager=callback_manager
    )
    set_global_service_context(service_context)

    retriever = _get_retriever(folder_name="prüfungsplan")

    chain = retriever.as_query_engine(
        service_context=service_context,
        similarity_top_k=3,
        verbose=True,
        text_qa_template=DEFAULT_TEXT_QA_PROMPT_CUSTOM,
    )

    results = chain.query(query)
    final_answer = str(results)

    llama_debug.flush_event_logs()
    return final_answer


# Pruefungsamt
def run_pruefungsamt_chain_llama_index(query: str) -> str:
    llm_predictor = LLMPredictor(llm=llm)

    service_context = ServiceContext.from_defaults(
        llm_predictor=llm_predictor, callback_manager=callback_manager
    )
    set_global_service_context(service_context)

    retriever = _get_retriever(folder_name="prüfungsamt")

    chain = retriever.as_query_engine(
        service_context=service_context,
        similarity_top_k=3,
        verbose=True,
        text_qa_template=DEFAULT_TEXT_QA_PROMPT_CUSTOM,
    )

    results = chain.query(query)
    final_answer = str(results)

    llama_debug.flush_event_logs()
    return final_answer


# Materialien Mathematik für Informatiker II
def run_mathematik_für_informatiker_ii_index(query: str) -> str:

    llm_predictor = LLMPredictor(llm=llm)

    service_context = ServiceContext.from_defaults(
        llm_predictor=llm_predictor, callback_manager=callback_manager
    )
    set_global_service_context(service_context)

    retriever = _get_retriever(
        folder_name="prüfungsmaterialien/mathematik_für_informatiker_ii"
    )
    chain = retriever.as_query_engine(
        service_context=service_context,
        similarity_top_k=3,
        verbose=True,
        text_qa_template=DEFAULT_TEXT_QA_PROMPT_CUSTOM,
    )

    results = chain.query(query)

    final_answer = str(results)

    llama_debug.flush_event_logs()
    return final_answer


# studiengang
def run_studiengang_chain_llama_index(query: str) -> str:
    llm_predictor = LLMPredictor(llm=llm)

    service_context = ServiceContext.from_defaults(
        llm_predictor=llm_predictor, callback_manager=callback_manager
    )

    set_global_service_context(service_context)

    retriever = _get_retriever(folder_name="studiengang")

    chain = retriever.as_query_engine(
        service_context=service_context,
        similarity_top_k=3,
        verbose=True,
        text_qa_template=DEFAULT_TEXT_QA_PROMPT_CUSTOM,
    )

    results = chain.query(query)

    final_answer = str(results)

    llama_debug.flush_event_logs()
    return final_answer


# prüfungen
def run_prüfungen_chain_llama_index(query: str) -> str:
    llm_predictor = LLMPredictor(llm=llm)

    service_context = ServiceContext.from_defaults(
        llm_predictor=llm_predictor, callback_manager=callback_manager
    )

    set_global_service_context(service_context)

    retriever = _get_retriever(folder_name="prüfungen")

    chain = retriever.as_query_engine(
        service_context=service_context,
        similarity_top_k=3,
        verbose=True,
        text_qa_template=DEFAULT_TEXT_QA_PROMPT_CUSTOM,
    )

    results = chain.query(query)

    final_answer = str(results)

    llama_debug.flush_event_logs()
    return final_answer


# praktikum
def run_praktikum_chain_llama_index(query: str) -> str:
    llm_predictor = LLMPredictor(llm=llm)

    service_context = ServiceContext.from_defaults(
        llm_predictor=llm_predictor, callback_manager=callback_manager
    )

    set_global_service_context(service_context)

    retriever = _get_retriever(folder_name="praktikum")

    chain = retriever.as_query_engine(
        service_context=service_context,
        similarity_top_k=3,
        verbose=True,
        text_qa_template=DEFAULT_TEXT_QA_PROMPT_CUSTOM,
    )

    results = chain.query(query)

    final_answer = str(results)

    llama_debug.flush_event_logs()
    return final_answer
